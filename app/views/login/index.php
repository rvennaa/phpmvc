<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome Back!</title>
</head>
<body>
<div class="container my-3">
    <div class="d-flex justify-content-center mt-2">
        <div class="col-xl-10 col-lg-8">
            <div class=" card border-0 shadow-lg">
                <div class="card-bodyp-0">
                    <div class="row">
                        <div class="col-lg-5" style="background-image: url('<?= BASE_URL; ?>/img/view1.jpeg'); background-size:cover;  background-position: center;"></div>
                        <div class="col-lg-6 p-5">
                            <div class="m-5">
                                <div class="mb-4 text-center">
                                    <h5>Log in</h5>
                                    <p class="text-secondary">Please Log In to continue</p>
                                </div>
                                <form action="<?= BASE_URL; ?>/login/auth" method="post">
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Username</label>
                                        <input type="text" class="form-control" placeholder="username" name="username">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Password</label>
                                        <input type="password" class="form-control" placeholder="password" name="password">
                                    </div>
                                    <div class="row mb-3">
                                        <div class="d-flex justify-content-center">
                                            <div class="form-check">
                                                <input type="checkbox" name="" id="" class="form-check-input">
                                                <label for="" class="form-check-label">Remember me</label>
                                            </div>
                                            <div class="col text-end">
                                                <a href="" class="text-decoration-none">Forgot Password</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button class="btn bg-primary text-light mb-3" style="width: 100%;">Sign In</button>
                                        <p class="text-secondary">New user? <a href="<?= BASE_URL; ?>/register" class="text-decoration-none">Sign Up</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>